from django import forms
from .models import MataKuliah

TERM = (
    ('Gasal 2019/2020', 'Gasal 2019/2020'),
    ('Genap 2019/2020', 'Genap 2019/2020'),
    ('Gasal 2020/2021', 'Gasal 2020/2021')
)

class MataKuliahForm(forms.ModelForm):

    class Meta:
        model = MataKuliah
        fields = (
            'namaMatkul', 'dosen', 'SKS', 'deskripsi', 'semester', 'ruang'
        )
    
    namaMatkul = forms.CharField(max_length=25, label='Nama Mata Kuliah')
    dosen = forms.CharField(max_length=50, label='Nama Dosen Pengampu')
    SKS = forms.IntegerField(label='Jumlah SKS')
    deskripsi = forms.TextInput()
    semester = forms.ChoiceField(choices=TERM, label='Semester')
    ruang = forms.CharField(max_length=20, label='Ruangan')