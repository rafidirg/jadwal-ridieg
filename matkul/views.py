from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import  MataKuliahForm
from .models import MataKuliah

# Create your views here.
def formMk(request):
    if request.method == "POST":
        formMatkul = MataKuliahForm(request.POST)
        if formMatkul.is_valid():
            formMatkul.save()
            return HttpResponseRedirect('/tabel')

    formMatkul = MataKuliahForm()
    return render(request, 'forms.html', {'formMatkul': formMatkul})


def tabelMK(request):
    matkuls = MataKuliah.objects.all()

    return render(request, 'table.html', {'matkuls': matkuls})

def matkul(request, matkul_pk):
    detil = MataKuliah.objects.get(id=matkul_pk)
    return render(request, 'detil.html', {'detil':detil})

def delet(request, deletdis):
    delet = MataKuliah.objects.get(id=deletdis)
    delet.delete()
    return HttpResponseRedirect('/tabel')